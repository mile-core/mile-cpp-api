#include <cstdarg>

#include <cstring>
#include <string>
#include "binary_serializer.h"
#include "crypto_types.h"
#include "crypto.h"
#include "csa_type.h"
#include "csa_cxx_light.h"
#include "csa_pdbg.h"
#include "sha3.h"

static void print_and_safe_error(char** errorMessage, const char* msg, ...)
{
    char buffer[1024] = {};
    va_list ap = {};

    va_start(ap, msg);
    vsnprintf(buffer, sizeof(buffer), msg, ap);
    va_end(ap);

    PDBG_ERR(buffer);

    size_t size = strlen(buffer);

    *errorMessage = (char *) malloc(size + 1);
    strncpy(*errorMessage, buffer, size);
    (*errorMessage)[size] = '\0';
}

pcsa_result generate_key_pair(pcsa_keys_pair* keyPair, char** errorMessage)
{
    if (keyPair == nullptr)
    {
        print_and_safe_error(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }
    memset(keyPair, 0, sizeof(pcsa_keys_pair));

    PrivateKey privateKey;
    PublicKey publicKey;
    CreateKeyPair(privateKey, publicKey);

    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::public_key) - 1);
    strncpy(keyPair->private_key, privateKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::private_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result generate_key_pair_from_private_key(pcsa_keys_pair* keyPair, char** errorMessage)
{
    if (keyPair == nullptr)
    {
        print_and_safe_error(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }
    memset(keyPair->public_key, 0, sizeof(pcsa_keys_pair::public_key));

    std::string errorDescription;
    PrivateKey privateKey;
    if (!privateKey.SetBase58CheckString(keyPair->private_key, errorDescription))
    {
        print_and_safe_error(errorMessage, "Error read private key: %s", errorDescription.c_str());
        return PCSA_RES_FAIL;
    }

    PublicKey publicKey;
    RestoreKeyPairFromPrivate(privateKey, publicKey);

    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::public_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result generate_key_pair_with_secret_phrase(pcsa_keys_pair* keyPair, const char* phrase, 
                                                 size_t length, char** errorMessage)
{
    if (keyPair == nullptr)
    {
        print_and_safe_error(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }

    if (phrase == nullptr || length == 0)
    {
        print_and_safe_error(errorMessage, "Internal error: secret thrase is empty");
        return PCSA_RES_FAIL;
    }

    memset(keyPair, 0, sizeof(pcsa_keys_pair));

    Seed seed;
    seed.Clear();
    sha3_256((const unsigned char*) phrase, length, seed.Data.data());

    PrivateKey privateKey;
    PublicKey publicKey;
    CreateKeyPair(privateKey, publicKey, seed);

    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::public_key) - 1);
    strncpy(keyPair->private_key, privateKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::private_key) - 1);

    return PCSA_RES_OK;
}

static pcsa_result light_transaction_prepair(const pcsa_keys_pair* keyPair,
                                           PublicKey* publicKey,
                                           PrivateKey* privateKey,
                                           const char* transactionId,
                                           uint64_t* ui,
                                           char** errorMessage)
{

    std::string error;
    if (!publicKey->SetBase58CheckString(keyPair->public_key, error))
    {
        print_and_safe_error(errorMessage, "Public key has invalide base58 encode: '%s'", error.c_str());
        return PCSA_RES_FAIL;
    }

    if (!privateKey->SetBase58CheckString(keyPair->private_key, error))
    {
        print_and_safe_error(errorMessage, "Private key has invalide base58 encode: '%s'", error.c_str());
        return PCSA_RES_FAIL;
    }

    if (!StringToUInt64(transactionId, *ui, false))
    {
        print_and_safe_error(errorMessage, "Can't get last transaction id from string");
        return PCSA_RES_FAIL;   
    }

    return PCSA_RES_OK;
}

pcsa_result create_transaction_register_node(const pcsa_keys_pair* keyPair, 
                                             const char* nodeAddress,
                                             const char* transactionId,
                                             char** transaction,
                                             char** errorMessage)
{
    if (keyPair == nullptr)
    {
        print_and_safe_error(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    size_t nodeAddressLength = strlen(nodeAddress);
    if (nodeAddress == nullptr || nodeAddressLength > 64 || nodeAddressLength == 0)
    {
        print_and_safe_error(errorMessage, "Node address length must be in [1..64] but length is '%zu'", nodeAddressLength);
        return PCSA_RES_FAIL;
    }

    if (transactionId == nullptr)
    {
        PDBG_ERR("Transaction id is not define");
        return PCSA_RES_FAIL;  
    }

    PublicKey publicKey;
    PrivateKey privateKey;
    uint64_t ui;
    if (light_transaction_prepair(keyPair, &publicKey, &privateKey, transactionId, &ui, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    unsigned char signedBit = 1;
    BSerializer serializer;
    serializer << ui + 1 << publicKey
               << setw(64) << setfill('\0') << nodeAddress
               << signDigest(privateKey, publicKey)
               << signedBit << signer;

    std::string trans{serializer.toHex()};

    *transaction = (char *) malloc(trans.length() + 1);
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result create_transaction_unregister_node(const pcsa_keys_pair* keyPair,
                                               const char* transactionId,
                                               char** transaction,
                                               char** errorMessage)
{
    if (keyPair == nullptr)
    {
        print_and_safe_error(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (transactionId == nullptr)
    {
        print_and_safe_error(errorMessage, "Transaction id is not define");
        return PCSA_RES_FAIL;  
    }

    PublicKey publicKey;
    PrivateKey privateKey;
    uint64_t ui;
    if (light_transaction_prepair(keyPair, &publicKey, &privateKey, transactionId, &ui, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    unsigned char signedBit = 1;
    BSerializer serializer;
    serializer << ui + 1 << publicKey
                << signDigest(privateKey, publicKey)
               << signedBit << signer;

    std::string trans{serializer.toHex()};

    *transaction = (char *) malloc(trans.length() + 1);
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result create_transaction_transfer_assets(const pcsa_keys_pair* srcKeyPair,
                                               const char* dstWalletPublicKey,
                                               const char* transactionId,
                                               unsigned short assets,
                                               const char* amount,
                                               char** transaction,
                                               char** errorMessage)
{

    if (srcKeyPair == nullptr)
    {
        print_and_safe_error(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (dstWalletPublicKey == nullptr)
    {
        print_and_safe_error(errorMessage, "Destination wallet private key is not define");
        return PCSA_RES_FAIL;  
    }

    if (transactionId == nullptr)
    {
        print_and_safe_error(errorMessage, "Transaction id is not define");
        return PCSA_RES_FAIL;  
    }

    if (amount == nullptr)
    {
        print_and_safe_error(errorMessage, "Ammunt is not define");
        return PCSA_RES_FAIL;  
    }


    PublicKey srcPublicKey;
    PrivateKey srcPrivateKey;
    uint64_t ui;

    if (light_transaction_prepair(srcKeyPair, &srcPublicKey, &srcPrivateKey, transactionId, &ui, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    PublicKey dstPublicKey;
    std::string errorDescription;
    if (!dstPublicKey.SetBase58CheckString(dstWalletPublicKey, errorDescription))
    {
        print_and_safe_error(errorMessage, "Public key has invalide base58 encode: '%s'", errorDescription.c_str());
        return PCSA_RES_FAIL;
    }

    blockchain_amount_data_t amountData;
    blockchain_amount_t amountType;
    if (!StringToBlockchainAmount(amount, amountType))
    {
        print_and_safe_error(errorMessage, "Could't parse amount string %s", amount);
        return PCSA_RES_FAIL;
    }
    SerializeBlockchainAmount(amountType, amountData);

    unsigned char signedBit = 1;

    BSerializer serializer;
    serializer << ui + 1 << assets << srcPublicKey
               << dstPublicKey << amountData
               << signDigest(srcPrivateKey, srcPublicKey)
               << signedBit << signer;

    std::string trans{serializer.toHex()};

    *transaction = (char *) malloc(trans.length() + 1);
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result  create_transaction_user_data(const pcsa_keys_pair* keyPair,
                                          const char* transactionId,
                                          const char* dataString,
                                          char** transaction,
                                          char** errorMessage)
{
    if (keyPair == nullptr)
    {
        print_and_safe_error(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (dataString == nullptr || *dataString == '\0')
    {
        print_and_safe_error(errorMessage, "User data is not define");
        return PCSA_RES_FAIL;  
    }

    if (transactionId == nullptr)
    {
        print_and_safe_error(errorMessage, "Transaction id is not define");
        return PCSA_RES_FAIL;  
    }

    std::vector<unsigned char> data;

    while (*dataString)
    {
        data.push_back((unsigned char) *dataString++);
    }
    
    PublicKey publicKey;
    PrivateKey privateKey;
    uint64_t ui;

    if (light_transaction_prepair(keyPair, &publicKey, &privateKey, transactionId, &ui, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    unsigned char signedBit = 1;
    BSerializer serializer;
    serializer << ui + 1 << publicKey
               << data
               << signDigest(privateKey, publicKey)
               << signedBit << signer;


    std::string trans{serializer.toHex()};

    *transaction = (char *) malloc(trans.length() + 1);
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}