#include <iostream>
#include <cstring>

#include "csa_cxx_light.h"

int main()
{
    pcsa_keys_pair keyPair;
    char* errorDescription = nullptr;

    if (generate_key_pair(&keyPair, &errorDescription))
    {
        std::cout << "Error happened in generate_key_pair";
        if (errorDescription)
        {
            std::cout << " error message: '" << errorDescription << "'" << std::endl;
            free(errorDescription);
        }
        return 1;
    }
    else
    {
        std::cout << "Public key: " << keyPair.public_key << std::endl;
        std::cout << "Public key: " << keyPair.private_key << std::endl;   
    }

    pcsa_keys_pair keyPairSecretMessage;
    errorDescription = nullptr;
    char sectetPhrase[] = "test string"; 
    if (generate_key_pair_with_secret_phrase(&keyPairSecretMessage, sectetPhrase, 
                                             sizeof(sectetPhrase), &errorDescription))
    {
        std::cout << "Error happened in generate_key_pair_with_secret_phrase";
        if (errorDescription)
        {
            std::cout << " error message: '" << errorDescription << "'" << std::endl;
            free(errorDescription);
        }
        return 1;
    }
    else
    {
        std::cout << "Public key: " << keyPairSecretMessage.public_key << std::endl;
        std::cout << "Public key: " << keyPairSecretMessage.private_key << std::endl;   
    }

    char* transaction = nullptr;
    errorDescription = nullptr;
    if (create_transaction_register_node(&keyPair, "hello", "0", &transaction, &errorDescription))
    {
        std::cout << "Error happened in create_transaction_register_node";
        if (errorDescription)
        {
            std::cout << " error message: '" << errorDescription << "'";
            free(errorDescription);
        }
    } 
    else
    {
        std::cout << "create_transaction_register_node: " << transaction << std::endl;
        free(transaction);
    }


    transaction = nullptr;
    errorDescription = nullptr;
    if (create_transaction_register_node(&keyPair, "teeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee11111", 
                                         "0", &transaction, &errorDescription))
    {
        std::cout << "Error happened in create_transaction_register_node";
        if (errorDescription)
        {
            std::cout << " error message: '" << errorDescription << "'" << std::endl;
            free(errorDescription);
        }
    } 
    else
    {
        std::cout << "create_transaction_register_node: " << transaction << std::endl;
        free(transaction);
    }

    transaction = nullptr;
    errorDescription = nullptr;
    if (create_transaction_unregister_node(&keyPair, "0", &transaction, &errorDescription))
    {
        std::cout << "Error happened in create_transaction_unregister_node";
        if (errorDescription)
        {
            std::cout << " error message: '" << errorDescription << "'" << std::endl;
            free(errorDescription);
        }
    } 
    else
    {
        std::cout << "create_transaction_unregister_node: " << transaction << std::endl;
        free(transaction);
    }

    transaction = nullptr;
    errorDescription = nullptr;
    if (create_transaction_transfer_assets(&keyPair, keyPairSecretMessage.public_key, 
                                           "0", 0, "1000", &transaction, &errorDescription))
    {
        std::cout << "Error happened in create_transaction_transfer_assets";
        if (errorDescription)
        {
            std::cout << " error message: '" << errorDescription << "'" << std::endl;
            free(errorDescription);
        }
    } 
    else
    {
        std::cout << "create_transaction_transfer_assets: " << transaction << std::endl;
        free(transaction);
    }

    transaction = nullptr;
    errorDescription = nullptr;
    if (create_transaction_user_data(&keyPair, "0", "olla", &transaction, &errorDescription))
    {
        std::cout << "Error happened in create_transaction_user_data";
        if (errorDescription)
        {
            std::cout << " error message: '" << errorDescription << "'" << std::endl;
            free(errorDescription);
        }
    } 
    else
    {
        std::cout << "create_transaction_user_data: " << transaction << std::endl;
        free(transaction);
    }

    errorDescription = nullptr;
    std::cout << "Public key: " << keyPair.public_key << std::endl;
    std::cout << "Private key: " << keyPair.private_key << std::endl; 
    std::cout << "Clear public key" << std::endl;   

    memset(keyPair.public_key, 0, sizeof(pcsa_keys_pair::public_key));  

    std::cout << "Public key: " << keyPair.public_key << std::endl;
    std::cout << "Private key: " << keyPair.private_key << std::endl; 

    if (generate_key_pair_from_private_key(&keyPair, &errorDescription))
    {
        std::cout << "generate_key_from_private" << std::endl;
    } else
    {
        std::cout << "Public key: " << keyPair.public_key << std::endl;
        std::cout << "Private key: " << keyPair.private_key << std::endl;
    }

    return 0;
}