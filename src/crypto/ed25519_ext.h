#ifndef ED25519_EXT_H
#define ED25519_EXT_H

#ifdef __cplusplus
extern "C" {
#endif
    
void ed25519_restore_from_private_key(unsigned char *public_key, const unsigned char *private_key);

#ifdef __cplusplus
}
#endif

#endif
