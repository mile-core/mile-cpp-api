#include "binary_serializer.h"
#include "crypto.h"

void BSerializer::setWidth(unsigned int width)
{
    m_width = width;
}

void BSerializer::setFill(char fill)
{
    m_fill = fill;
}

unsigned int BSerializer::getWidth()
{
    return m_width;
}

char BSerializer::getFill()
{
    return m_fill;
}

void BSerializer::setSignature(const Signature& sign)
{
    m_signature = sign;
}

Signature& BSerializer::getSignature()
{
    return m_signature;
} 

void BSerializer::do_setw(BSerializer& serializer, unsigned int width)
{
    serializer.setWidth(width);
}

void BSerializer::do_setFill(BSerializer& serializer, char fill)
{
    serializer.setFill(fill);
}

void BSerializer::do_signDigest(BSerializer& serializer, PrivateKey privateKey, PublicKey publicKey)
{
    Signer signer(privateKey, publicKey);
    Digest digest;
    CalculateDigest(serializer.m_data, digest);
    Signature signature;
    signer.SignDigest(digest, signature);
    serializer.setSignature(signature);
}

BSerializer& BSerializer::operator<<(const std::vector<unsigned char>& data)
{
    m_data.insert(m_data.end(), data.begin(), data.end());
    return *this;
}


/*BSerializer& BSerializer::operator<<(const uint256_t& data)
{
    std::vector<unsigned char> temp;
    UInt256ToVector(data, temp);

    std::move(temp.begin(), temp.end(), std::back_inserter(m_data));

    return *this;
}*/

BSerializer& BSerializer::operator<<(const uint64_t& data)
{
    std::vector<unsigned char> temp;
    UInt64ToVector(data, temp);

    std::move(temp.begin(), temp.end(), std::back_inserter(m_data));

    return *this;
}

BSerializer& BSerializer::operator<<(unsigned char data)
{
    m_data.push_back(data);
    return *this;
}

BSerializer& BSerializer::operator<<(unsigned int data)
{
    m_data.push_back(static_cast<unsigned char>(data & 0xff));
    m_data.push_back(static_cast<unsigned char>((data >> 8) & 0xff));
    m_data.push_back(static_cast<unsigned char>((data >> 16) & 0xff));
    m_data.push_back(static_cast<unsigned char>((data >> 24) & 0xff));
    return *this;
}

BSerializer& BSerializer::operator<<(unsigned short data)
{
    m_data.push_back(static_cast<unsigned char>(data & 0xff));
    m_data.push_back(static_cast<unsigned char>((data >> 8) & 0xff));
    return *this;
}

BSerializer& BSerializer::operator<<(const char* str)
{
    int i = 0;
    while (*str)
    {
        m_data.push_back(*str++);
        ++i;
    }

    if (getWidth() != 0)
    {
        if (m_width > i)
        {
            std::fill_n(std::back_inserter(m_data), m_width - i, m_fill);
        }
        setfill(0);
        setWidth(0);
    }
    return *this;
}

BSerializer& BSerializer::operator<<(BSerializer& (&call)(BSerializer&))
{
    return call(*this);
}

std::string BSerializer::toHex()
{
    return BinToHex(m_data);
}

UnaryModifier<unsigned int> setw(unsigned int width)
{
    return UnaryModifier<unsigned int>(BSerializer::do_setw, width);
}

UnaryModifier<char> setfill(char fill)
{
    return UnaryModifier<char>(BSerializer::do_setFill, fill);
}

BinaryModifier<PrivateKey, PublicKey> signDigest(const PrivateKey& privateKey, const PublicKey& publicKey)
{
    return BinaryModifier<PrivateKey, PublicKey>(BSerializer::do_signDigest, privateKey, publicKey);
}

BSerializer& signer(BSerializer& signer)
{
    return signer << signer.getSignature(); 
}