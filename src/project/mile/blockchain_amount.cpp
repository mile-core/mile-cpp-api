#include <sstream>

#ifdef USE_R128_FIXEDPOINT
    #include <cstdint>
    #define R128_IMPLEMENTATION
#endif
#include "blockchain_amount.h"

#ifdef USE_R128_FIXEDPOINT

string BlockchainAmountToString(const blockchain_amount_t& amount)
{
    char buffer[64];
    int size = r128ToString(buffer, 64, &amount);
    return string(buffer);
}

bool is_number(const std::string& s)
{
    bool delimiter = false;
    for (std::string::const_iterator it = s.begin(); it != s.cend(); ++it)
    {
        if (*it == '.')
        {
            if (delimiter)
            {
                return false;
            }
            delimiter = true;
            continue;
        }

        if (!std::isdigit(*it))
        {
            return false;
        }
        
    }
    return !s.empty();
}

bool StringToBlockchainAmount(const string& blockchainAmountString, blockchain_amount_t& amount)
{
    if (!is_number(blockchainAmountString))
    {
        return false;
    }

    //char *endPointer = 0;
    r128FromString(&amount, blockchainAmountString.c_str(), nullptr);
    //size_t processedSize = static_cast<size_t>(endPointer - blockchainAmountString.c_str());
    //return processedSize == blockchainAmountString.size();
    return true;
}

void SerializeBlockchainAmount(const blockchain_amount_t& amount, blockchain_amount_data_t& data)
{
    // serialize part after comma (lo)
    uint64_t v = amount.lo;
    for (size_t i = 0; i < 8; i++, v >>= 8) // 8 bytes
    {
        data.Data[i] = static_cast<unsigned char>(v & 0xff);
    }
    // serialize part before comma (hi)
    v = amount.hi;
    for (size_t i = 0; i < 8; i++, v >>= 8) // 8 bytes
    {
        data.Data[i+8] = static_cast<unsigned char>(v & 0xff);
    }
}

void DeserializeBlockchainAmount(const blockchain_amount_data_t& data, blockchain_amount_t& amount)
{
    // deserialize part after comma (lo)
    uint64_t v = 0;
    for (size_t i = 0; i < 8; i++) // 8 bytes
    {
        v |= static_cast<uint64_t>(data.Data[i]) << (i * 8);
    }
    amount.lo = v;
    // deserialize part before comma (hi)
    v = 0;
    for (size_t i = 0; i < 8; i++) // 8 bytes
    {
        v |= static_cast<uint64_t>(data.Data[i+8]) << (i * 8);
    }
    amount.hi = v;
}

#else // boost multiprecision

string BlockchainAmountToString(const blockchain_amount_t& amount)
{
    std::stringstream ss;
    ss << std::setprecision(std::numeric_limits<blockchain_amount_t>::max_digits10) << amount;
    return ss.str();
}

bool StringToBlockchainAmount(const string& blockchainAmountString, blockchain_amount_t& amount)
{
    std::stringstream ss;
    ss << std::dec << blockchainAmountString;
    ss >> amount;
    // check that all symbols are processed e.g. 0.123456789abc -> error
    if (ss.fail() || !ss.eof())
    {
        return false;
    }
    return true;
}

void SerializeBlockchainAmount(const blockchain_amount_t& amount, blockchain_amount_data_t& data)
{
    // export into 8-bit unsigned values, most significant bit first
    cpp_int intNumber = cpp_int(amount.backend().bits()); // temporary int number
    // OutputIterator export_bits(const number<const cpp_int_backend<MinBits, MaxBits, SignType, Checked, Allocator>, ExpressionTemplates>& val,
    //                            OutputIterator out, unsigned chunk_size, bool msv_first = true);
//    export_bits(intNumber, data.Data.begin(), 8);
//
    //
    size_t size = intNumber.backend().size();
    const boost::multiprecision::limb_type* p = intNumber.backend().limbs();
    size_t limbTypeSize = sizeof(boost::multiprecision::limb_type);
    //
    size_t n = 0; // data pointer
    // note: except exponent field length
    size_t mantissaDataLength = eBlockchainAmountDataSize-2;
    //
    for (size_t i = 0; i < size && n < mantissaDataLength; ++i, ++p)
    {
        boost::multiprecision::limb_type t = *p;
        //
        for (size_t j = 0; j < limbTypeSize && n < mantissaDataLength; j++)
        {
            data.Data[n++] = t & 0xff;
            t >>= 8;
        }
    }

    while (n < mantissaDataLength)
    {
        data.Data[n++] = 0; // padding with zeros
    }
  
    // check zeros after 12 bytes? e.g. size=2 limbTypeSize=8 -> 16 bytes!
    // grab the exponent as well from boost::int16_t
    unsigned short e = static_cast<unsigned short>(amount.backend().exponent());
    // serialize exponent in little endian format
    data.Data[eBlockchainAmountDataSize - 2] = static_cast<unsigned char>(e & 0xff);
    data.Data[eBlockchainAmountDataSize - 1] = static_cast<unsigned char>((e >> 8) & 0xff);
}

void DeserializeBlockchainAmount(const blockchain_amount_data_t& data, blockchain_amount_t& amount)
{
    // import amount
    // we have to proceed via an intermediate integer
    cpp_int intNumber = 0;

    // note: except exponent field length
    size_t mantissaDataLength = eBlockchainAmountDataSize-2;

    // process data in little endian format
    // note: optimize later
    for (size_t i = 0, offset = 0; i < mantissaDataLength; i++, offset += 8) // note: use const
    {
        intNumber |= static_cast<cpp_int>(data.Data[i]) << offset;
    }
    //import_bits(i, data.begin(), data.begin() + 8);
    amount = static_cast<blockchain_amount_t>(intNumber);
    // process exponent in little endian format
    boost::int16_t e = static_cast<boost::int16_t>(static_cast<unsigned short>(data.Data[eBlockchainAmountDataSize-2]) | (static_cast<unsigned short>(data.Data[eBlockchainAmountDataSize-1]) << 8));
    amount.backend().exponent() = e;
}

#endif
