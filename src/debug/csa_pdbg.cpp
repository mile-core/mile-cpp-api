#include <stdarg.h>

#include <unordered_set>
#include <string>
#include <string.h>

#include "csa_pdbg.h"
#include "csa.h"

static std::unordered_set<std::string> group_set;

void pcsa_pdbg(char const *group, char const *file, int line, char const *fmt, ...)
{
    if (group_set.find(std::string{group}) == group_set.end() &&
        group_set.find(std::string{"all"}) == group_set.end())
    {
        return;
    }

    char buffer[1024] = "";
    snprintf(buffer, sizeof(buffer), "[%s] %s:%d: ", group, file, line);
    size_t length = (size_t)strlen(buffer);
    char* p = buffer + length;

    va_list ap;

    va_start(ap, fmt);
    vsnprintf(p, sizeof(buffer) - length, fmt, ap);
    va_end(ap);

#ifdef ANDROID
    __android_log_print(ANDROID_LOG_VERBOSE, "JNI", "%s", buffer);
#else
    printf("%s\n", buffer);
#endif
}

void pcsa_pdbg_set_groups(char const *groups)
{
    char const *p, *p_end;
    p = groups;
    do
    {
        p_end = strchr(p, ',');
        if(p_end)
        {
            group_set.insert(std::string{p, std::string::size_type(p_end - p)});
            p = p_end + 1;
        }
        else
        {
            group_set.insert(std::string{p});
        }
    }
    while(p_end);
}