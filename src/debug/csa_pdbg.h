/*
 * csa_pdbg.h
 */

#ifndef CSA_PDBG_H_
#define CSA_PDBG_H_

#ifdef ANDROID
#include <android/log.h>
#endif

#ifdef DEBUG_BUILD
// general form
#define PDBG_(group, fmt, ...) pcsa_pdbg(group, __FILE__, __LINE__, fmt, ##__VA_ARGS__)
// for aux debug messages
#define PDBG(fmt, ...) pcsa_pdbg("debug", __FILE__, __LINE__, fmt, ##__VA_ARGS__)
// for error debug messages
#define PDBG_ERR(fmt, ...) pcsa_pdbg("error", __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#else	// DEBUG_BUILD
#define PDBG_(group, fmt, ...)
#define PDBG(fmt, ...)
#define PDBG_ERR(fmt, ...)
#endif	// DEBUG_BUILD


void pcsa_pdbg(char const *group, char const *file, int line, char const *fmt, ...);

#endif /* CSA_PDBG_H_ */
