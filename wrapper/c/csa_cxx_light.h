#pragma once

#include <stdint.h>
#include "csa_type_light.h"

#ifdef __cplusplus
extern "C"
{
#endif

pcsa_result generate_key_pair(pcsa_keys_pair* keyPair, char** errorMessage);
pcsa_result generate_key_pair_from_private_key(pcsa_keys_pair* keyPair, char** errorMessage);
pcsa_result generate_key_pair_with_secret_phrase(pcsa_keys_pair* keyPair, const char* phrase, 
                                                 size_t length, char** errorMessage);
pcsa_result create_transaction_register_node(const pcsa_keys_pair* keyPair, 
                                             const char* nodeAddress,
                                             const char* transactionId,
                                             char** transaction, 
                                             char** errorDescription);
pcsa_result create_transaction_unregister_node(const pcsa_keys_pair* keyPair,
                                               const char* transactionId,
                                               char** transaction, 
                                               char** errorDescription);

pcsa_result create_transaction_transfer_assets(const pcsa_keys_pair* srckeyPair,
                                               const char* dstWalletPublicKey,
                                               const char* transactionId,
                                               unsigned short assets,
                                               const char* amount,
                                               char** transaction,
                                               char** errorDescription);

pcsa_result  create_transaction_user_data(const pcsa_keys_pair* keyPair,
                                          const char* transactionId,
                                          const char* dataString,
                                          char** transaction,
                                          char** errorDescription);
    

#ifdef __cplusplus
}
#endif