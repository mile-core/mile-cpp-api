#pragma once

#include "csa_type_light.h"
extern "C" {
static const unsigned int big_number_length = 128;
static const unsigned int big_length_string = 100;
static const unsigned int super_big_length_string = 200;

// binary data package size
/*static const unsigned int reg_node_trans_len        = 197; 
static const unsigned int ureg_node_trans_len       = 129; 
static const unsigned int trans_assets_trans_s_len  = 167; 
static const unsigned int trans_assets_trans_us_len = 135; */

static const unsigned int assets_length = 30;
static const unsigned int digest_len = (32 + 3) * BASE58MULTY + 4; 

typedef struct
{
    char name[big_length_string];
} pcsa_string_literal;

typedef struct
{
    unsigned int        code;
    pcsa_string_literal asset_name; 
} pcsa_supported_asset;

typedef struct
{
    char description[super_big_length_string];
    char digest[digest_len + 1];
    char type[big_length_string];
} pcsa_wallet_transaction;

typedef struct
{
    pcsa_supported_asset* assets;
    unsigned int assets_length;
    char tags[big_length_string];
    char node_address[big_length_string];
    char last_transaction_id[big_number_length];
    char exist;
} pcsa_wallet_state;

typedef struct
{
    char block_count[big_number_length];
    char last_block_digest[big_number_length];
    char last_block_merkle_root[big_number_length];
    char transaction_count[big_number_length];
    char node_count[big_number_length];
    char local_node;
    char non_empty_wallet_count[big_number_length];
    char voting_transaction_count[big_number_length];
    char pending_transaction_count[big_number_length];
    char blockchain_state[big_number_length];
    char* synchronization_state;
    char consensus_round[big_number_length];
    char* voting_nodes;
} pcsa_blockchain_state;

typedef struct
{
    char project_name[big_length_string];
    char version[big_length_string];
    pcsa_string_literal *supported_transactions;
    unsigned int transactions_count;
    pcsa_supported_asset *supported_assets;
    unsigned int assets_count;
} pcsa_blockchain_info;
}