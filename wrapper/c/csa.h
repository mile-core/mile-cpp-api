/*
 * csa.h
 *
 * C style API to Control System services.
 */
#pragma once
#include "csa_type.h"

#ifdef __cplusplus
extern "C" {
#endif

//**************************************************************************************************
// GENERAL
//**************************************************************************************************
const char * pcsa_get_version_str();

void pcsa_on_connect(void (*on_connect)(int code, const char *msg));
void pcsa_on_auth(void (*on_auth)(int code, const char *msg));

void pcsa_destroy(void);

pcsa_result pcsa_get_last_cs_result();
void pcsa_get_last_cs_error_text(char *buf, unsigned buf_size);
void pcsa_clear_last_cs_error_text();


const char * pcsa_result_to_str(pcsa_result r);

/*
 *  client_type     shell|web|snmp
 */
pcsa_result pcsa_connect(const char* ip_address,
                            unsigned short port,
                            const char* user_name,
                            const char* client);

pcsa_result pcsa_disconnect(void);

/*
 * Функция для управления печатью отладочной информации библиотеки - задает группы отладочных
 * сообщений, которые будут выводиться на консоль.
 *
 * groups   Which message groups are enabled to output. "all" - print all debug messages.
 *          Ex.: "debug|error"
 */
void pcsa_pdbg_set_groups(char const *groups);

#ifdef __cplusplus
}
#endif