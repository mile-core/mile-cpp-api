Pod::Spec.new do |s|

  s.name         = "MileCsaLight"
  s.version      = "0.4"
  s.summary      = "MileCsaLight is a SDK to create Mile Core Wallet"
  s.description  = "MileCsaLight is a SDK helps to generate Mile wallet keys: Public and Private. Also the SDK gives api to sign client transactions."                   

  s.homepage     = "https://mile.global"

  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.authors            = { "denis svinarchuk" => "denn.nevera@gmail.com" }
  s.social_media_url   = "https://mile.global"

  s.platform     = :ios
  s.platform     = :osx

  s.ios.deployment_target = "10.0"
  s.osx.deployment_target = "10.12"

  s.source       = { :git => "https://bitbucket.org/mile-core/mile-cpp-api", :tag => "#{s.version}" }

  s.source_files  = "platforms/mile-ios-sdk/mile-ios-sdk/Classes/*.{h}", 
                    "platforms/mile-ios-sdk/mile-ios-sdk/Classes/**/*.{h,m,mm,swift}",
                    "src/**/*.{h,c,hpp,cpp}",
                    "wrapper/c/*.h"
  s.exclude_files = "src/test", 
                    "src/external/ed25519/*.dll", 
                    "src/external/ed25519/test.c",
                    "src/external/r128/test"

  s.public_header_files = "platforms/mile-ios-sdk/mile-ios-sdk/Classes/*.{h,hpp}" 

  s.frameworks = "Foundation"
  s.libraries  = 'c++'

  s.requires_arc = true

  s.compiler_flags = '-Wno-format', '-x objective-c++', '-DNDEBUG', '-DUSE_R128_FIXEDPOINT', '-DR128_STDC_ONLY'

  s.xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => 'CSA=1' , 'OTHER_CFLAGS' => ''}
  
end
