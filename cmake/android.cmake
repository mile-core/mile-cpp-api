target_include_directories(${LIBSUMUS} PRIVATE
                           ${boost_DIR}/include/boost-1_65_1)

# write lib version
file(WRITE "${VERSION_FILE}" "${LIBSUMUS} ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")
message(STATUS "${LIBSUMUS} version ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")

target_link_libraries( # Specifies the target library.
                       ${LIBSUMUS} ${log-lib}
                       lib_boost_system lib_boost_thread)