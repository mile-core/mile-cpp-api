message(STATUS "Compiler: ${CMAKE_CXX_COMPILER_ID}")

set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON) 

find_package(Boost REQUIRED COMPONENTS thread regex)
include_directories(${Boost_INCLUDE_DIRS})

# compare compiler ID string
string(COMPARE EQUAL "${CMAKE_CXX_COMPILER_ID}" "MSVC" MSVCfound)

if (MSVCfound)
    # Microsoft Visual Studio 2015
    # note: suppress warning etc
    # warning C4005: '__useHeader': macro redefinition
    # note: suppress error in pcap-stdinc.h (#define inline __inline)
    # xkeycheck.h(250): fatal error C1189: #error:  The C++ Standard Library forbids macroizing keywords.
    # _SCL_SECURE_NO_WARNINGS suppress warnings with any_of() expression etc
    # NOMINMAX is needed to disable min/max macros in windows header and to enable proper work of std::numeric_limits<>::max() function
    # note: cmake doesn't support proper long line stripping into multilines
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /EHsc /D_CRT_SECURE_NO_WARNINGS /D_SCL_SECURE_NO_WARNINGS /wd4005 /D_XKEYCHECK_H /DNOMINMAX /DWIN32 /DWPCAP /DHAVE_REMOTE /DBOOST_THREAD_USE_LIB /D_WIN32_WINNT=0x0501")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /DX86_WINDOWS /DMAJOR_VERSION=${MAJOR_VERSION} /DMINOR_VERSION=${MINOR_VERSION}")
    set(CMAKE_CXX_FLAGS_RELEASE "/MT")
    set(CMAKE_CXX_FLAGS_DEBUG "/MTd")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /EHsc /D_CRT_SECURE_NO_WARNINGS /D_SCL_SECURE_NO_WARNINGS /wd4005 /D_XKEYCHECK_H /DNOMINMAX /DWIN32 /DWPCAP /DHAVE_REMOTE /DBOOST_THREAD_USE_LIB /D_WIN32_WINNT=0x0501 /DX86_WINDOWS")
    set(CMAKE_C_FLAGS_RELEASE "/MT")
    set(CMAKE_C_FLAGS_DEBUG "/MTd")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /MANIFEST:NO")
else()
    # use MinGW compiler by default
    # note: cmake doesn't support proper long line stripping into multilines
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -DWIN32 -DWPCAP -DHAVE_REMOTE -DBOOST_THREAD_USE_LIB -D_WIN32_WINNT=0x0501")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DX86_WINDOWS -static-libgcc -static-libstdc++")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DX86_WINDOWS -static-libgcc -static-libstdc++")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -s") # strip binary
endif()

target_link_libraries(${LIBSUMUS} ${CMAKE_THREAD_LIBS_INIT} ${Boost_LIBRARIES})