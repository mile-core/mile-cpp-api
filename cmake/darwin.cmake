set(CMAKE_CXX_FLAGS "-std=c++11 -DDARWIN -I/usr/local/opt/openssl/include")

# write libsumus version
file(WRITE "${VERSION_FILE}" "${LIBSUMUS} ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")
message(STATUS "${LIBSUMUS} version ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")

target_link_libraries(${LIBSUMUS} ${CMAKE_THREAD_LIBS_INIT})

add_executable(Test src/test/test.cpp)
add_test(NAME Test COMMAND Test)
set_target_properties(Test PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}" )

target_link_libraries (Test ${LIBSUMUS})
