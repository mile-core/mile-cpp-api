add_executable(Test src/test/test.cpp)
target_link_libraries (Test ${LIBSUMUS})
add_test(NAME Test COMMAND Test)
set_target_properties(Test
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}"
)