find_package(Boost REQUIRED COMPONENTS system thread)
find_package(Threads)

include_directories (
    ${Boost_INCLUDE_DIRS}
)

if (DEFINED DEBUG_BUILD AND "${DEBUG_BUILD}" STREQUAL "1")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fsanitize=leak -fsanitize=undefined")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -s -O2")
endif()


set(CMAKE_CXX_FLAGS "-fPIC -std=c++11 -DX86_LINUX ")
set(CMAKE_C_FLAGS "-fPIC")

# write libsumus version
file(WRITE "${VERSION_FILE}" "${LIBSUMUS} ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")
message(STATUS "${LIBSUMUS} version ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")

target_link_libraries(${LIBSUMUS} ${CMAKE_THREAD_LIBS_INIT} ${Boost_LIBRARIES} rt)
