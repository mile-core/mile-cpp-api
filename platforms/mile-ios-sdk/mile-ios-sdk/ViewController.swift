//
//  ViewController.swift
//  mile-ios-sdk
//
//  Created by denis svinarchuk on 18.06.2018.
//  Copyright © 2018 Mile Core. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        do {
            
            let pks = try MileCsa.generateKeys();
            
            Swift.print("PKS: , pks")
            
            Swift.print("Register   node:",  try MileCsa.registerNode(pks, address: "hello", transactionId: "0"))
            Swift.print("Unregister node: ", try MileCsa.unregisterNode(pks,transactionId: "0"))
            
            let destPks = try MileCsa.generateKeys()
            
            Swift.print("Transfer       : ", try MileCsa.createTransfer(pks, 
                                                                        destPublicKey: destPks.publicKey, 
                                                                        transactionId: "0", 
                                                                        assets: 0, 
                                                                        amount: "0"))
            
            
            let pks_sph1 = try MileCsa.generateKeys(withSecretPhrase: "фраза какая-то")            
            let pks_sph2 = try MileCsa.generateKeys(withSecretPhrase: "фраза какая-то")
            let pks_sph3 = try MileCsa.generateKeys(withSecretPhrase: "фраза какая-то другая")
            
            let pks_sph4 = try MileCsa.generateKeys(fromPrivateKey: pks_sph3.privateKey)

            
            Swift.print(" pks with secret phrase1 = \(pks_sph1, pks_sph1==pks_sph2) ")
            Swift.print(" pks with secret phrase2 = \(pks_sph2, pks_sph1.privateKey==pks_sph2.privateKey) ")
            Swift.print(" pks with secret phrase3 = \(pks_sph3, pks_sph1==pks_sph3) ")

            Swift.print(" pks with private key phrase4 = \(pks_sph4, pks_sph3==pks_sph4) ")
            
            Swift.print("Transfer       : ", try MileCsa.createTransfer(pks, 
                                                                        destPublicKey: "WTF!KEY", 
                                                                        transactionId: "0", 
                                                                        assets: 0, 
                                                                        amount: "0"))            
        }
        catch let error {
            Swift.print("Error: \(error)")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

