//
//  MileWalletBridge.h
//  mile-ios-sdk
//
//  Created by denis svinarchuk on 18.06.2018.
//  Copyright © 2018 Mile Core. All rights reserved.
//


#import <Foundation/Foundation.h>

#define NSMileErrorDomain @"NSMileErrorDomain"

@interface MileCsaKeys : NSObject
@property(readonly) NSString * _Nonnull publicKey;
@property(readonly) NSString * _Nonnull privateKey;

-(nonnull instancetype) initWith:(nonnull NSString*)publicKey privateKey:(nonnull NSString*)privateKey;
+ (BOOL) testPublicKey:(nonnull NSString*)publicKey;

@end

@interface MileCsa : NSObject

+(nullable MileCsaKeys*)generateKeys:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error;

+(nullable MileCsaKeys*)generateKeysWithSecretPhrase:(nonnull NSString*)phrase error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error;

+(nullable MileCsaKeys*)generateKeysFromPrivateKey:(nonnull NSString*)privateKey error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error;

+(nullable NSString*) registerNode:(nonnull MileCsaKeys *)keyPair address:(nonnull NSString*)address transactionId:(nonnull NSString*)transactionId error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error;  

+(nullable NSString*) unregisterNode:(nonnull MileCsaKeys *)keyPair transactionId:(nonnull NSString*)transactionId error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error;  

+(nullable NSString*) createTransfer:(nonnull MileCsaKeys *)srcKeyPair destPublicKey:(nonnull NSString*)destPublicKey transactionId:(nonnull NSString*)transactionId assets:(unsigned short)asstes amount:(nonnull NSString*)amount error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error;  

@end

