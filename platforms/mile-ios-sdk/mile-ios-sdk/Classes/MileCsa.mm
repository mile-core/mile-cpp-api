//
//  MileWalletBridge.m
//  mile-ios-sdk
//
//  Created by denis svinarchuk on 18.06.2018.
//  Copyright © 2018 Mile Core. All rights reserved.
//

#include <stdio.h>
#import "csa_cxx_light.h"
#import "MileCsa.h"

@interface MileCsaKeys()
@property(readonly) pcsa_keys_pair *keysPair;
@end

@implementation MileCsaKeys
{
    pcsa_keys_pair _keysPair;
}

@synthesize publicKey  = _publicKey;
@synthesize privateKey = _privateKey;

+ (BOOL) testPublicKey:(NSString *)publicKey {
       
    if ([publicKey length]<=1) {
        return NO;
    }
    
    if ([publicKey length]<=50 && [publicKey length]>=40 ) {
        return YES;
    }
    return NO;
}

- (nonnull instancetype) initWith:(NSString *)publicKey privateKey:(NSString *)privateKey{
    self = [super init];
    if (self){
        _publicKey = [publicKey copy];
        _privateKey = [privateKey copy];
        
        memset(&_keysPair, 0, sizeof(_keysPair));
        
        memcpy(_keysPair.public_key, [_publicKey UTF8String], sizeof(_keysPair.public_key));    
        memcpy(_keysPair.private_key, [_publicKey UTF8String], sizeof(_keysPair.private_key));    

    }
    return self;
}

- (pcsa_keys_pair*) keysPair {
    return &_keysPair;
}

- (BOOL) isEqual:(id)object {
    return [self.privateKey isEqual:[(MileCsaKeys*)object privateKey]];
} 

- (NSString*) description {
    return [[NSString alloc] initWithFormat:@"%@:%@", _publicKey, _privateKey];
}

@end

@implementation MileCsa

static inline void dumpKeys(pcsa_keys_pair *keys, MileCsaKeys *keyPair) {
    memcpy(keys->public_key, [[keyPair publicKey] UTF8String], sizeof(keys->public_key));    
    memcpy(keys->private_key, [[keyPair privateKey] UTF8String], sizeof(keys->private_key));    
}

static inline NSString *string2NSString(char *string) {
    if (string) {
        NSString *result = [[NSString alloc] initWithUTF8String:string];
        free(string);
        return result;
    }
    return nil;
}

static inline NSError *error2NSError(char *errorMessage, pcsa_result code, NSString *description){
    if (errorMessage) {            
        NSDictionary *userInfo = @{
                                   NSLocalizedDescriptionKey: NSLocalizedString([NSString stringWithUTF8String:errorMessage], nil),
                                   NSLocalizedFailureReasonErrorKey: NSLocalizedString(description, nil)
                                   };            
        NSError *error = [NSError errorWithDomain:NSMileErrorDomain code:(NSInteger)(code) 
                                 userInfo:userInfo];
        free(errorMessage);
        return error;
    }
    return nil;
}

+ (nullable MileCsaKeys*) generateKeys:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error{
    pcsa_keys_pair keys;
    char *errorMessage = nullptr;
    pcsa_result code;
        
    if ( (code = generate_key_pair(&keys, &errorMessage)) == PCSA_RES_OK) {
        return  [[MileCsaKeys alloc] 
                 initWith:[NSString stringWithUTF8String:keys.public_key] 
                 privateKey:[NSString stringWithUTF8String:keys.private_key]];
    }
    else {
        *error = error2NSError(errorMessage, code, @"Key generator error");
    }    
    return nil;
}

+ (nullable MileCsaKeys*) generateKeysWithSecretPhrase:(NSString *)phrase error:(NSError *__autoreleasing  _Null_unspecified *)error {
    pcsa_keys_pair keys;
    char *errorMessage = nullptr;
    pcsa_result code;

    if((code = generate_key_pair_with_secret_phrase(&keys, 
                                                    [phrase UTF8String], [phrase length], &errorMessage))==PCSA_RES_OK){        
        return  [[MileCsaKeys alloc] 
                 initWith:[NSString stringWithUTF8String:keys.public_key] 
                 privateKey:[NSString stringWithUTF8String:keys.private_key]];
    }
    else {
        *error = error2NSError(errorMessage, code, @"Key by secret phrase generator error");
    }
    
    return nil;
    
}

+ (nullable MileCsaKeys*) generateKeysFromPrivateKey:(NSString *)privateKey error:(NSError *__autoreleasing  _Null_unspecified *)error {
    pcsa_keys_pair keys;
    char *errorMessage = nullptr;
    pcsa_result code;
    
    memcpy(keys.private_key, [privateKey UTF8String], sizeof(keys.private_key));    
    
    if((code = generate_key_pair_from_private_key(&keys, 
                                                  &errorMessage))==PCSA_RES_OK){        
        return  [[MileCsaKeys alloc] 
                 initWith:[NSString stringWithUTF8String:keys.public_key] 
                 privateKey:[NSString stringWithUTF8String:keys.private_key]];
    }
    else {
        *error = error2NSError(errorMessage, code, @"Key by secret phrase generator error");
    }
    
    return nil;
}

+ (nullable NSString*) registerNode:(MileCsaKeys *)keyPair 
                   address:(NSString *)address 
             transactionId:(NSString *)transactionId 
                     error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error
{    
    pcsa_keys_pair keys;  dumpKeys(&keys, keyPair);
    char *errorMessage = nullptr;
    char *transaction = nullptr;
    pcsa_result code;

    if ( (code=create_transaction_register_node(&keys, 
                                         [address UTF8String], 
                                         [transactionId UTF8String], 
                                         &transaction, &errorMessage)) == PCSA_RES_OK){
        return string2NSString(transaction);
    }
    else {
        *error = error2NSError(errorMessage, code, @"Register node transaction error");
    }    
    return nil;
}

+ (nullable NSString*) unregisterNode:(MileCsaKeys *)keyPair 
               transactionId:(NSString *)transactionId  
                       error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error{
    pcsa_keys_pair keys;  dumpKeys(&keys, keyPair);
    char *errorMessage = nullptr;
    char *transaction = nullptr;
    pcsa_result code;

    if((code=create_transaction_unregister_node(&keys, [transactionId UTF8String], &transaction, &errorMessage)) == PCSA_RES_OK) {
       return string2NSString(transaction);
    }
    else {
        *error = error2NSError(errorMessage, code, @"Unregister node transaction error");
    }    
    return nil;
}

+ (nullable NSString*) createTransfer:(MileCsaKeys *)srcKeyPair 
                        destPublicKey:(NSString *)destPublicKey 
                        transactionId:(NSString *)transactionId 
                               assets:(unsigned short)assets 
                               amount:(NSString *)amount 
                                error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error{
    
    pcsa_keys_pair keys;  dumpKeys(&keys, srcKeyPair);
    
    char *errorMessage = nullptr;
    char *transaction = nullptr;
    pcsa_result code;

    if((code=create_transaction_transfer_assets(&keys,
                                          [destPublicKey UTF8String],
                                          [transactionId UTF8String], 
                                          assets, 
                                          [amount UTF8String], 
                                          &transaction, 
                                          &errorMessage)) == PCSA_RES_OK){
       return string2NSString(transaction);      
    }
    else {
        *error = error2NSError(errorMessage, code, @"Create transfer transaction error");
    }    
    return nil;
}

@end
